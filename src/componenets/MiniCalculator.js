import React, {Component} from 'react';
import './miniCalculator.less';

class MiniCalculator extends Component {
  constructor(props){
    super(props);
    this.state = {
      value: 0
    }
    this.handleChange = this.handleValueInput.bind(this)
  }

  handleValueInput(event){
    console.log("result: " + this.state.value);
    let operator = '';
    let length = document.getElementsByTagName("button").length;
    for (let i = 0; i < length; i++) {
      if (event.target == document.getElementsByTagName("button")[i]) {
        operator = document.getElementsByTagName("button")[i].innerText;
      }
    }
    console.log(event.target)
    if(operator === '加1'){this.state.value += 1;}
    if(operator === '减1'){this.state.value -= 1;}
    if(operator === '乘以2'){this.state.value *= 2;}
    if(operator === '除以2'){this.state.value /= 2;}
    console.log(" new result: " + this.state.value);
    this.setState({value: this.state.value})
  }

  render() {
    return (
      <section>
        <div>计算结果为:
          <span className="result"> {this.state.value} </span>
        </div>
        <div className="operations">
          <button onClick={this.handleChange}>加1</button>
          <button onClick={this.handleChange}>减1</button>
          <button onClick={this.handleChange}>乘以2</button>
          <button onClick={this.handleChange}>除以2</button>
        </div>
      </section>
    );
  }
}

export default MiniCalculator;

